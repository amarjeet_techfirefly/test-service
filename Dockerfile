FROM adoptopenjdk/openjdk11:alpine-jre
Volume /tmp
ADD /target/*.jar test-service.jar
ENTRYPOINT ["java","-jar","/test-service.jar"]